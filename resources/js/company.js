

require('./bootstrap');

window.Vue = require('vue');

Vue.component('add-job-component', require('./company/AddJob.vue').default);
Vue.component('jobs-component', require('./company/Jobs.vue').default);


const app = new Vue({
    el: '#company',
});
