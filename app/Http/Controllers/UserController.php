<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct() {

        $this->middleware('auth');
    }

    public function index()
    {
        return view('student.index');

    }

    public function get_data()
    {
        $user = User::where('id', Auth::id())->with('cvs')->first();
        return $user;
    }

    public function store_cover_letter(Request $request)
    {
        $user_id = Auth::id();
        $user = User::where('id', $user_id)->update(['cover_letter' => $request->cover_letter]);
        return $request->cover_letter;
    }

}
