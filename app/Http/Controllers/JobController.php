<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{

    public function __construct() {

        $this->middleware('auth:company',['except'=>['get_active_jobs','get_active_jobs_view']]);
        $this->middleware('auth',['only'=>['get_active_jobs','get_active_jobs_view']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company.get_jobs');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $job = Job::create([
            "title" => $request->title,
            "description" => $request->description,
            "min_salary" => $request->min_salary,
            "max_salary" => $request->max_salary,
            "active" => $request->active,
            "company_id" => Auth::guard('company')->user()->id
        ]);

        return $job;
    }

    public function update(Request $request, Job $job)
    {
        $job = Job::where('id', $request->id)->update(['active' => $request->active]);
        return $job;
    }

    public function get_jobs()
    {
        $jobs = Job::where('company_id', Auth::guard('company')->user()->id)->get();

        return $jobs;
    }

    public function get_active_jobs()
    {
        $jobs = Job::where('active', 1)->with('company')->get();

        return $jobs;
    }

    public function get_active_jobs_view()
    {
        return view('student.get_jobs');
    }

}
