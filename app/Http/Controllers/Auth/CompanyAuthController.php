<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use Illuminate\Support\Facades\Auth;

class CompanyAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:company', ['except' => ['logout']]);
    }


    public function showRegisterForm()
    {
        return view('companyauth.register');
    }
    public function showLoginForm()
    {
        return view('companyauth.login');
    }
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        Company::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        if (Auth::guard('company')->attempt(['email' => $request['email'],'password' => $request['password']])) {
            return redirect()->intended(route('company.dashboard'));
        }
        return redirect()->back()->withInput($request->only('email','name'));
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        if (Auth::guard('company')->attempt([
            'email' => $request['email'],
            'password' => $request['password']
        ], $request->remember)) {
            return redirect()->intended(route('company.dashboard'));
        }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function logout()
    {
        Auth::guard('company')->logout();
        return redirect('/home');
    }
}




