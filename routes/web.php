<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\User;


Route::get('/', 'HomeController@index')->name('home');


Route::prefix('student')->group(function () {
    // Laravel Views Routes
    Route::get('/profile', 'UserController@index')->name('student.dashboard');
    Route::get('/active_jobs', 'JobController@get_active_jobs_view')->name('student.active_jobs');;

    // Vue Requests Routes
    Route::get('/data', 'UserController@get_data');
    Route::post('/cv', 'CvController@store');
    Route::post('/cover_letter', 'UserController@store_cover_letter');
    Route::get('/active_jobs_data', 'JobController@get_active_jobs');
});



Route::prefix('company')->group(function () {
    // Login and Registration Routes
    Route::get('/register', 'Auth\CompanyAuthController@showRegisterForm')->name('company.register');
    Route::post('/register', 'Auth\CompanyAuthController@register')->name('company.register.submit');
    Route::get('/login', 'Auth\CompanyAuthController@showLoginForm')->name('company.login');
    Route::post('/login', 'Auth\CompanyAuthController@login')->name('company.login.submit');
    Route::post('/logout', 'Auth\CompanyAuthController@logout')->name('company.logout');

    // Laravel view routes
    Route::get('/', 'CompanyController@index')->name('company.dashboard');
    Route::get('/add_job', 'JobController@create')->name('company.add_job');

    // Vue Requests Routes
    Route::get('/data', 'CompanyController@get_data');
    Route::post('/add_job', 'JobController@store');
    Route::post('/toggle_active', 'JobController@update');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
